import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
int width = 800;
int height = 600;
color red, blue, green, white;
QT qt;
List<Shape> shapes = new ArrayList();
List<Shape> found = new ArrayList();
Circle userCircle;

void settings() {
  size(width, height);
}

void setup() {
  //fullScreen();

  background(0);
  red = color(255, 0, 0);
  blue = color(0, 0, 255);
  green = color(0, 255, 0);
  white = color(255);
  for (int i = 0; i < 100; i++) {
    PVector randPV = new PVector(random(200, width - 200), random(200, height - 200));
    float randS = 40;
    Circle randShape = new Circle(randPV, randS);
    shapes.add(randShape);
  }
  //PVector userC = new PVector(mouseX, mouseY);
  //userCircle = new Circle(userC, 50);
}

void draw() {
  //background(0);
  //fill(0, 5);
  //noStroke();
  //rect(0, 0, width, height);
  // construct the quadtree
  PVector bcenter = new PVector(width/2, height/2);
  Rectangle boundary = new Rectangle(bcenter, width, height);
  qt = new QT(boundary, 10);
  qt.insertMultiple(shapes);
  // user circle
  //userCircle.setC(new PVector(mouseX, mouseY));
  //userCircle.draw(green);
  
  for (Shape shape : shapes) {
    found.clear();
    qt.query(shape, found);
    if(found.size() > 0 ) {
      shape.draw(red);
    }
    else {
      shape.draw();
    }
    
    shape.update();
  } 
  //
  //qt.query(userCircle, found);
  //for (Shape shape : found) {
  //  shape.draw(red);
  //}
  println(frameRate);
}
void mouseClicked() {
  //saveFrame("line-######.png");
}
