public abstract class Shape {
  abstract boolean intersects(Rectangle that);
  abstract boolean intersects(Circle that);
  abstract boolean intersects(PVector that);
  abstract boolean intersects(Point that);
  abstract boolean intersects(Shape that);
  float orientation = degrees(random(360));
  PVector vel = PVector.fromAngle(this.orientation);
  PVector c;
  List<Shape> intersecting = new ArrayList<Shape>();
  abstract void draw(color col);
  void draw() {
    this.draw(255);
  }
  
  void setC(PVector vector) {
    this.c = vector;
  }
  
  void setAcc(float acc) {
    this.vel.setMag(acc);
  }

  void update() {
    this.setC(PVector.add(this.c, this.vel));
    this.intersecting.clear();
  }
  
  void addIntersecting(Shape shape) {
    if(!shape.intersecting.contains(this)) {
      this.intersecting.add(shape);
    }
  }
}
