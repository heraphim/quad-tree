public class Rectangle extends Shape {
  float w;
  float h;
  float hw;
  float hh;
  PVector nw;
  PVector ne;
  PVector se;
  PVector sw;
  boolean regular;

  Rectangle(PVector center, float w, float h) {

    this.c = center;
    this.w = w;
    this.h = h;
    this.hw = this.w / 2;
    this.hh = this.h /2;
    this.nw = new PVector(this.c.x - this.hw, this.c.y - this.hh);
    this.ne = new PVector(this.c.x + this.hw, this.c.y - this.hh);
    this.se = new PVector(this.c.x + this.hw, this.c.y + this.hh);
    this.sw = new PVector(this.c.x - this.hw, this.c.y + this.hh);
    this.regular = this.w == this.h ? true : false;
  }

  void draw(color colr) {
    noFill();
    strokeWeight(1);
    stroke(colr);
    rect(this.nw.x, this.nw.y, this.w, this.h);
  }

  boolean intersects(Shape that) {
    boolean ret = false;
    if (that instanceof Circle) {
      ret = this.intersects((Circle) that);
    } else if (that instanceof Rectangle) {
      ret = this.intersects((Rectangle) that);
    } else if (that instanceof Point) {
      ret = this.intersects((Point) that);
    }
    return ret;
  }

  boolean intersects(Rectangle rect) {
    if ((this.c.x - this.hw > rect.c.x + rect.hw) || (this.c.x + this.hw < rect.c.x - rect.hw) || (this.c.y - this.hh > rect.c.y + rect.hh) || (this.c.y + this.hh < rect.c.y - rect.hh)) {
      return false;
    }
    return true;
  }
  boolean intersects(Circle circle) {
    float dx = abs(circle.c.x - this.c.x);
    float dy = abs(circle.c.y - this.c.y);
    if (dx > (this.hw + circle.r)) {
      return false;
    }
    if (dy > (this.hh + circle.r)) { 
      return false;
    }
    if (dx <= (this.hw)) { 
      return true;
    } 
    if (dy <= (this.hh)) { 
      return true;
    }
    float cdsq = ((dx - this.hw) * (dx - this.hw)) + ((dy - this.hh) * (dy - this.hh));
    return (cdsq <= circle.r * circle.r);
  }
  boolean intersects(Point point) {
    if (point.c.x > this.c.x + this.hw || point.c.x < this.c.x - this.hw || point.c.y > this.c.y + this.hh || point.c.y < this.c.y - this.hh) {
      return false;
    }
    return true;
  }

  boolean intersects(PVector vector) {
    if (vector.x > this.c.x + this.w/2 || vector.x < this.c.x - this.w /2 || vector.y > this.c.y + this.h/2 || vector.y < this.c.y - this.w/2) {
      return false;
    }
    return true;
  }
}
