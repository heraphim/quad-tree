class Point extends Shape {
  Point() {
    this.c = new PVector(random(width), random(height));
  }  

  Point(PVector point) {
    this.c = point;
  }

  void draw(color colr) {
    noFill();
    strokeWeight(5);
    stroke(colr);
    point(this.c.x, this.c.y);
  }

  boolean intersects(Shape that) {
    boolean ret = false;
    if (that instanceof Circle) {
      ret = this.intersects((Circle) that);
    } else if (that instanceof Rectangle) {
      ret = this.intersects((Rectangle) that);
    } else if (that instanceof Point) {
      ret = this.intersects((Point) that);
    }
    return ret;
  }

  boolean intersects(PVector that) {
    return (this.c.x == that.x && this.c.y == that.y);
  }

  boolean intersects(Point that) {
    return (this.c.x == that.c.x && this.c.y == that.c.y);
  }
  boolean intersects(Rectangle that) {
    return that.intersects(this);
  }

  boolean intersects(Circle that) {
    return that.intersects(this);
  }
}
