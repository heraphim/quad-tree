public class QT {
  int cap;
  int pointNo;
  int insideNo;
  Rectangle b;
  List<Point> points = new ArrayList();
  List<Shape> found = new ArrayList();
  List<Circle> circles = new ArrayList();
  List<Circle> foundC = new ArrayList();
  List<Shape> inside = new ArrayList();
  QT nw;
  QT ne;
  QT se;
  QT sw;
  boolean divided = false;
  QT(Rectangle boundary, int cap) {
    this.b = boundary;
    this.cap = cap;
  }

  void query(Shape queryingShape, List<Shape> found) {
    if (!queryingShape.intersects(this.b)) {
      return;
    } else {
      for (Shape shape : this.inside) {
        if (!queryingShape.equals(shape) && !found.contains(shape) && queryingShape.intersects(shape)) {
            found.add(shape);
            queryingShape.addIntersecting(shape);
        }
      }
      if (this.divided) {
        this.nw.query(queryingShape, found);
        this.ne.query(queryingShape, found);
        this.sw.query(queryingShape, found);
        this.se.query(queryingShape, found);
      }
    }
  }


  void draw() {
    this.b.draw();
    for (Shape shape : this.inside) {
      shape.draw();
    }
    if (this.divided) {
      this.nw.draw();
      this.ne.draw();
      this.se.draw();
      this.sw.draw();
    }
  }

  void draw(color colr) {
    this.b.draw(colr);
    //for (Shape shape : this.inside) {
    //  shape.draw(colr);
    //}
    if (this.divided) {
      this.nw.draw(colr);
      this.ne.draw(colr);
      this.se.draw(colr);
      this.sw.draw(colr);
    }
  }

  public void insertMultiple(List<Shape> shapes) {
    for (Shape shape : shapes) {
      this.tryInsert(shape);
    }
  }

  public void tryInsert(Shape shape) {
    if (this.contains(shape)) {
      if (this.insideNo < this.cap) {
        this.inside.add(shape);
        this.insideNo++;
      } else {
        tryInsertInChildren(shape);
      }
    }
  }

  private boolean contains(Shape shape) {
    if (this.b.intersects(shape)) {
      return true;
    }
    return false;
  }

  private void tryInsertInChildren(Shape shape) {
    if (!this.divided) {
      this.divide();
    }
    this.se.tryInsert(shape);
    this.ne.tryInsert(shape);
    this.sw.tryInsert(shape);
    this.nw.tryInsert(shape);
  }

  void divide() {
    float qw = this.b.w/4;
    float qh = this.b.h/4;
    float hw = this.b.w/2;
    float hh = this.b.h/2;
    PVector nwc = new PVector(this.b.c.x - qw, this.b.c.y - qh);
    Rectangle nw = new Rectangle(nwc, hw, hh);
    this.nw = new QT(nw, this.cap);
    PVector nec = new PVector(this.b.c.x + qw, this.b.c.y - qh);
    Rectangle ne = new Rectangle(nec, hw, hh);
    this.ne = new QT(ne, this.cap);
    PVector sec = new PVector(this.b.c.x + qw, this.b.c.y + qh);
    Rectangle se = new Rectangle(sec, hw, hh);
    this.se = new QT(se, this.cap);
    PVector swc = new PVector(this.b.c.x - qw, this.b.c.y + qh);
    Rectangle sw = new Rectangle(swc, hw, hh);
    this.sw = new QT(sw, this.cap);
    this.divided = true;
  }
}
