public class Circle extends Shape {
  float r;
  float d;
  PVector o;
  Circle(PVector center, float r) {
    this.setC(center);
    this.r = r;
    this.d = r * 2;
    this.setAcc(2);
  }

  void draw(color colr) {
    noFill();
    strokeWeight(1);
    stroke(colr);
    //ellipse(this.c.x, this.c.y, this.d, this.d);
    for(Shape shape : intersecting) {
      stroke(colr, 15);
      line(this.c.x, this.c.y, shape.c.x, shape.c.y);    
    }
    //line(this.c.x, this.c.y, this.o.x, this.o.y);
  }

  boolean intersects(Shape that) {
    boolean ret = false;
    if (that instanceof Circle) {
      ret = this.intersects((Circle) that);
    } else if (that instanceof Rectangle) {
      ret = this.intersects((Rectangle) that);
    } else if (that instanceof Point) {
      ret = this.intersects((Point) that);
    }
    return ret;
  }

   boolean intersects(Rectangle that) {
    return that.intersects(this);
  }

  boolean intersects(Circle circle) {
    if (dist(this.c.x, this.c.y, circle.c.x, circle.c.y) > this.r + circle.r) {
      return false;
    }
    return true;
  }

  boolean intersects(Point point) {
    if (dist(this.c.x, this.c.y, point.c.x, point.c.y) > this.r) {
      return false;
    }
    return true;
  }
  boolean intersects(PVector vector) {
    if (dist(this.c.x, this.c.y, vector.x, vector.y) > this.r) {
      return false;
    }
    return true;
  }
}
